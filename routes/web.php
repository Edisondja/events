<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::post('create_user','UserController@create');
Route::get('/users','UserController@index');
Route::get('/search_user/{search}','UserController@search_users');
Route::post('/delete_user','UserController@destroy');
Route::post('/update_user','UserController@update');


Route::post('/create_ticket','TicketsController@create');
Route::post('/update_ticket','TicketsController@update');
Route::post('/delete_ticket','TicketsController@destroy');
Route::get('/search_ticket/{search}','TicketsController@search_ticket');
Route::get('/tickets','TicketsController@index');
Route::get('/my_tickets_order','TicketsController@load_order_ticket');

Route::get('/organizer_registrer','OrganizerController@create');
Route::post('/organizer_delete','OrganizerController@dostroy');
Route::post('/organizer_update','OrganizerController@update');
Route::get('/organizer_search/{search}','OrganizerController@search_organize');

Route::get('/orders','OrderController@index');
Route::post('/order_create','OrderController@create');
Route::get('/my_orders','OrderController@my_orders');
Route::post('/delete_order','OrderController@destroy');
Route::post('/update_order','OrderController@update');




Route::get('/registrar', 'ControllerPersona@index');
Route::post('/guardarp', 'ControllerPersona@create');
Route::get('/enviar_correo', 'ControllerPersona@enviar_correo');
Route::get('/ver_archivos', 'GuardarArchivos@index');
Route::post('/guardar_archivos', 'GuardarArchivos@save');
Route::get('/capturar_token', 'ControllerPost@capturar_token');
