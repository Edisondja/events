<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use DB;
class TicketsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            return  DB::table("tickets")->orderBy("id","desc")->get();
    }

    public function search_ticket($name){

        $result = DB::table("tickets")->where("name","like","%".$name."%");
        return $result;
    }

    public function load_order_tickets($ticket_id){

        $tickets = DB::table('orders')->where('ticket_id',$ticket_id)->get();
        return $tickets;

    }

    public function create(Request $data)
    {

        $ticket = new App\Ticket();
        $ticket->name = $data->input('name');
        $ticket->description = $data->input('description');
        $ticket->start_date = $data->input('start_date');
        $ticket->end_date = $data->input('end_date');
        $ticket->target = $data->input('target');
        $ticket->organizer_id = $data->input('organizer_id');
        $ticket->user_id =  $data->input('user_id');
        $ticket->category_id = $data->input('category_id');
        $ticket->recurrent = $data->input('recurrent');
        $ticket->quantity = $data->input('quantity');
        $ticket->numer_of_people = $data->input('number_of_people');
        $ticket->location =  $data->input('location');
        $ticket->save();
    }


    public function show($id)
    {

        return App\Ticket::find($id);

    }


    public function update(Request $data)
    {
        $ticket = App\Ticket::find($data->input('ticket_id'));
        $ticket->name = $data->input('name');
        $ticket->description = $data->input('description');
        $ticket->start_date = $data->input('start_date');
        $ticket->end_date = $data->input('end_date');
        $ticket->target = $data->input('target');
        $ticket->organizer_id = $data->input('organizer_id');
        $ticket->user_id =  $data->input('user_id');
        $ticket->category_id = $data->input('category_id');
        $ticket->recurrent = $data->input('recurrent');
        $ticket->quantity = $data->input('quantity');
        $ticket->numer_of_people = $data->input('number_of_people');
        $ticket->location =  $data->input('location');
        $ticket->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(App\Ticket::find($id)->delete()){

            return response()->json(['state'=>'success']);
        }
    }
}
