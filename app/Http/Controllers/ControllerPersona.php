<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use Mail;

class ControllerPersona extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('test');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $p)
    {
        $persona = new App\Persona();
        $persona->nombre = $p->nombre;
        $persona->apellido = $p->apellido;
        $persona->save();

        return 'Registro guardado con exito';
    }

    public function enviar_correo(){

         Mail::send('correo', ['user' => 'Zeus'], function($m){

            $m->from('edisondja@gmail.com', 'UltraGame');
            $m->to('edisondja@gmail.com','Edison De Jesus Abreu')->subject('Your Reminder!');

        });

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
