<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ControllerPost extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $publicaciones = DB::table('posts')->limit(30);
        return $publicaciones;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $data)
    {
        $video = new App\post();
        $video->titulo = $data->titulo;
        $video->descripcion = $data->descripcion;
        $video->categoria = $data->categoria;
        $video->tag = $data->tag;
        $archivo_video = $data->file('video');
        //creando avatar de la vista previa
        {
           //donde ffmpeg va coger la foto del arhivo de video
           $avantar_video = time() . '' . $archivo_video->getClientOriginalName();
           Storage::disk('imaganes')->put($avantar_video, File::get($archivo_video));
        }
    }

    public function show($id)
    {
        $publicacion = App\post::find($id);
        return $publicacion;
    }

    public function capturar_token(Request $data)
    {
        return  $data->bearerToken();
    }

    public function update(Request $data)
    {
        $publicacion = App\post::find($data->id_post);
        $publicacion->titulo = $data->titulo;
        $publicacion->descripcion = $data->descripcion;
        $publicacion->categoria = $data->categoria;
        if ($data->poster !== '') {
            $arc = $data->file('poster');
            $nombre = time() . '' . $arc->getClientOriginalName();
            Storage::disk('imagenes')->put($nombre, \File::get($arc));
        }
        $publicacion->save();
        return 'Publicacion actualizada con exito';
    }


    public function consumir_token(){

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
