<?php

namespace App\Http\Controllers;
use DB;
use App\Organizer;
use League\Flysystem\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class OrganizerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  DB::table('organizers')->limit(50)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $data)
    {

        $organizer = new App\Organizer();
        $organizer->name = $data->input('name');
        $organizer->username = $data->input('username');
        $organizer->password = $data->input('password');
        $organizer->bio = $data->input('bio');
        $organizer->address = $data->input('address');
        $organizer->rnc = $data->input('rnc');
        $organizer->website = $data->input('website');
        $organizer->contact = $data->input('contact');
        $this->save_image($data,$organizer);
        $organizer->save();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function save_image($data,$organizer)
    {
        if($data->file('image')!==""){

            $img_name =time()."".$data->file('image')->getClientOriginalName();

            Storage::disk('imagenes')->put($img_name, \File::get($data->file('image')));
            $organizer->profile_img ="images/".$img_name;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search_organizer($search)
    {
        $result = DB::table("organizers")->where('name','like',"%$search%");
        return  $result;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $data)
    {
        $organizer = App\Organizer::find($data->user_id);
        $organizer->name = $data->input('name');
        $organizer->username = $data->input('username');
        $organizer->password = $data->input('password');
        $organizer->bio = $data->input('bio');
        $organizer->address = $data->input('address');
        $organizer->rnc = $data->input('rnc');
        $organizer->website = $data->input('website');
        $organizer->contact = $data->input('contact');
        $this->save_image($data,$organizer);
        $organizer->save();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            DB::table("organizers")->where("id",$id)->delete();
            return response()->json(['state'=>'success']);

    }
}
