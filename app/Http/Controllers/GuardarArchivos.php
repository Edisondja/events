<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class GuardarArchivos extends Controller
{
    public function index()
    {
        return View('subir');
    }

    public function save(Request $archivo)
    {
        $arc = $archivo->file('archivo');
        $nombre = $arc->getClientOriginalName();

        Storage::disk('public')->put($nombre, \File::get($arc));

        return '!!!!';
    }
}
