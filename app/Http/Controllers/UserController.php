<?php

namespace App\Http\Controllers;
use DB;
use App\User;
use Exception;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $result = DB::table('users')->orderBy('id', 'desc')->limit(50)->get();
            return $result;
        }catch(Exception $e){

            return response()->json(['state'=>$e->getMessage()]);

        }
    }

    public function search_user($name){

            $users = DB::table('users')->where('name','like',"%".$name."%");
            return $users;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $data)
    {
        try {
            $user = new App\User();
            $user->name = $data->input('name');
            $user->phone = $data->input('phone');
            $user->username = $data->input('username');
            $user->password = $data->input('password');
            $user->save();
            return response()->json(['state'=>'success']);

        }catch(Exception $e){

                return response()->json(['state'=>$e->getMessage()]);
        }
    }

    public function show($id)
    {
        return  App\User::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $user = App\User::find($data->input('user_id'));
            $user->name = $data->input('name');
            $user->phone = $data->input('phone');
            $user->username = $data->input('username');
            $user->password = $data->input('password');
            $user->save();
            return response()->json(['state'=>'success']);

        }catch(Exception $e){

                return response()->json(['state'=>$e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            App\User::find($id)->delete();
            return response()->json(['state'=>'success']);
        }catch(Exception $e){

            return response()->json(['state'=>$e->getMessage()]);
        }
    }
}
