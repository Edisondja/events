<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use DB;
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = DB::table("orders")->orderBy('id','desc')->limit(30)->get();
        return  $results;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $data)
    {
            $order = new App\Order();
            $order->user_id = $data->input('user_id');
            $order->ticket_id = $data->input('ticket_id');
            $order->quantity = $data->input('quantity');
            $order->total = ($data->input('total') * $data->input('quantity'));
            $order->tax = $data->input('tax');
            $order->refunde = $data->input('refunde');
            $order->save();
    }

    public function my_orders($order_id){

        $orders = DB::table("orders")->where('id',$order_id)->get();
        return $orders;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return  App\Order::find($id);
    }

    public function update(Request $data)
    {
        $order = App\Order::find($data->input('order_id'));
        $order->user_id = $data->input('user_id');
        $order->ticket_id = $data->input('ticket_id');
        $order->quantity = $data->input('quantity');
        $order->total = ($data->input('total') * $data->input('quantity'));
        $order->tax = $data->input('tax');
        $order->refunde = $data->input('refunde');
        $order->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        App\Order::find($id)->delete();

        return response()->json([
            ['state'=>'success']
        ]);
    }
}
