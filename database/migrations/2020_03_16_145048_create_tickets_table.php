<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('description');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('target');
            $table->integer('organizer_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('organizer');
            $table->integer('category_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('category');
            $table->bool('recurrent');
            $table->integer('quantity');
            $table->integer('number_of_people');
            $table->string('location');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
